from django.shortcuts import render, redirect
from .forms import RegisterForm
from django.contrib.auth import authenticate, login

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['password'])  # here the password is hashed
            user.save()
            return redirect('login_page')
    else:
        form = RegisterForm()
    return render(request, 'registration_app/register.html', {'form': form})

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            # Return an 'invalid login' error message.
            return render(request, 'registration_app/login.html', {'error': 'Invalid login'})
    else:
        return render(request, 'registration_app/login.html')

def home(request):
    return render(request, 'registration_app/home.html')